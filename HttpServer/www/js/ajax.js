$(function(){
	
	// Load Messages after load page
	loadMessages();
	
	// ADD MESSAGE
	$("#message-send").click(function(){
		var message = $("#message").val();
		if (message !== "" && message !== undefined) {
			AsyncAjaxRequest("","POST","?m="+message,function(response) {
				var messages = response.messages;
				if(messages.length <= 0){
					$("#message-container").append("No messages yet!");
				}else{
					loadMessages();
				}
			});
		}else{
			alert("empty message!");
		}	
	});
	
	// DELETE WALL
	$("#wall-delete-btn").click(function(){
		
		AsyncAjaxRequest("","DELETE","",function(response) {
			if(response.sucess){
				alert("The wall have been deleted");
				document.location.href="/";
			}else{
				alert("Error when trying delete wall");
			}
		});
		
	});
	
	// Clear message box contente
	$("#message-reset").click(function(){
		$("#message").val('');
	});
	
});


// https://www.w3schools.com/xml/ajax_xmlhttprequest_send.asp
// https://stackoverflow.com/questions/15847292/function-that-return-a-value-from-ajax-call-request
function loadMessages(){
	AsyncAjaxRequest("","GET","?format=json",function(response) {
		var name = response.name;
		var messages = response.messages;
		var date = response.date;
		$("#wall-title").html(name + " Wall");
		$("#wall-date").html(date);
		if(messages.length <= 0){
			$("#message-container").append("No messages yet!");
		}else{
			var i = 0;
			// clean message space
			$("#message-container").html('');
			for(var message in messages){
				var index = i;
				var content = messages[index].content;
				$("#message-container").append(buildMessageHtml(messages[index],index));
				i++;
			}
			
		}
		registMessageDelete();
    });
	
}

function AsyncAjaxRequest(url, method, options, response) {
	var request = new XMLHttpRequest();
	request.open(method, url + options, true);
	request.onreadystatechange = function() {
		if (request.readyState == XMLHttpRequest.DONE) {
			response(JSON.parse(request.responseText));
		}
	}
  	request.send(null);
}

// deprecated
function SyncAjaxRequest(url, method, options){
	var request = new XMLHttpRequest();
	request.open(method, url + options, false);
	request.send();
	return JSON.parse(request.responseText);
}

function registMessageDelete(){

	// DELETE MESSAGE
	$(".delete-message").click(function(){
		var id = $(this).attr('id');
		if (id >= 0) {
			AsyncAjaxRequest("","DELETE",window.location.href+"/"+id,function(response) {
				var messages = response.messages;
				if(messages.length <= 0){
					$("#message-container").html("No messages yet!");
				}else{
					loadMessages();
				}
			});
		}else{
			alert("empty message!");
		}	
	});
}

function buildMessageHtml(message, id){
	return 	`<li class="list-group-item">
					<div class="message-content col-xs-10 col-md-10"> `+message.content+`</div>
					<div class="message-buttons col-xs-2 col-md-2"><button id="`+id+`" type="button" class="btn btn-danger delete-message"> Remove </button></div>
			 </li>`;
}