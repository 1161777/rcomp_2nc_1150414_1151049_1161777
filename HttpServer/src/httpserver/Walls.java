package httpserver;

import java.util.HashMap;

/**
 *
 * @author marcio
 */
public class Walls {
    
    private static Walls instance;
    private static HashMap<String, Wall> walls = new HashMap();
    
    private Walls(){}
    
    public static Walls getInstance(){
        if(instance == null){
            synchronized (Walls.class) {
                if(instance == null){
                    instance = new Walls();
                }
            }
        }
        return instance;
    }
    
    public static Wall getWall(String name){
        Wall wall = walls.get(name);
        if(wall == null){
            wall = new Wall(name);
            walls.put(name, wall);
        }
        return wall;
    }
    
    public static Wall deleteWall(String wall_name){
        return walls.remove(wall_name);
    }
    
   
    
}
