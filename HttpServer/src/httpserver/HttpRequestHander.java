package httpserver;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.net.Socket;
import java.util.HashMap;
/**
 *
 * @author ANDRE MOREIRA (asc@isep.ipp.pt)
 */
public class HttpRequestHander extends Thread {

    private String baseFolder;
    private Socket sock;
    private DataInputStream inS;
    private DataOutputStream outS;
    private Walls walls;

    public HttpRequestHander(Socket s, String f) {
        this.baseFolder = f;
        this.sock = s;
        walls = Walls.getInstance();
    }

    public void run() {
        try {
            outS = new DataOutputStream(sock.getOutputStream());
            inS = new DataInputStream(sock.getInputStream());
        } catch (IOException ex) {
            System.out.println("Thread error on data streams creation");
        }

        try {
            HTTPmessage request = new HTTPmessage(inS);
            HTTPmessage response = new HTTPmessage();
            
            String[] url_resources = request.getURI().split("/");
            
            // se preenche as condições entra
            if(request.getURI().startsWith("/walls/")
                    && url_resources.length >=3 
                    && url_resources.length <=4){
                
                //getParams
                String resources = java.net.URLDecoder.decode(url_resources[2], "UTF-8");
                String[] expload = resources.split("\\?");   
                String wall_name = expload[0];
                
                HashMap<String, String> params = new HashMap();
                if(expload.length > 1){
                    params = getParams(expload[1]);
                }
                
                int message_id = -1;
                try{message_id = parseInt(url_resources[3]);}
                catch (Exception e){
                    message_id = -1;
                }
                                 
                // WALLS
                // cria um se não existir
                Wall wall = Walls.getWall(wall_name);

                // LIST ALL MESSAGES IN WALL
                if (request.getMethod().equals("GET") && message_id == -1) {
                    String format = params.get("format");
                    if(format != null && format.equalsIgnoreCase("json")){
                        response.setContentFromString(wall.toJson(), "text/json");
                    }else{
                        response.setContentFromFile(baseFolder + "/" + "wall.html");
                    }
                    response.setResponseStatus("200 Ok");
                }
                
                // ADD MESSAGE TO WALL
                if (request.getMethod().equals("POST")) {
                    String m = params.get("m");
                    if(m != null && m.length() > 0){
                        Message message = new Message(m);
                        wall.addMessage(message);
                        response.setContentFromString(wall.toJson(), "text/json");
                        response.setResponseStatus("201 Created");
                    }else{
                        response.setResponseStatus("500 Nok");
                    }
                }
                
                // DELETE MESSAGE TO WALL
                if (request.getMethod().equals("DELETE") && message_id >= 0) {
                    wall.deleteMessage(message_id);
                    response.setContentFromString(wall.toJson(), "text/json");
                    response.setResponseStatus("202  Accepted");
                }
                
                // DELETE WALL
                if (request.getMethod().equals("DELETE") && message_id == -1) {
                    walls.deleteWall(wall_name);
                    response.setContentFromString("{\"sucess\":true}", "text/json");
                    response.setResponseStatus("202 Ok");
                }
                
            }else if(request.getURI().startsWith("/walls/")){
                response.setContentFromString(
                                "<html><body><h1>Forbiden</h1></body></html>",
                                "text/html");
                response.setResponseStatus("503 Forbiden");
                //response.send(outS);
            }else{
                String fullname=baseFolder + "/";
                // GET FILE URL
                if(request.getURI().equals("/")){
                    fullname=fullname+"index.html";
                }else{
                    fullname=fullname+request.getURI();
                }
                if(response.setContentFromFile(fullname)) {
                    response.setResponseStatus("200 Ok");
                }else{
                    response.setContentFromString(
                                "<html><body><h1>Page not found</h1></body></html>",
                                "text/html");
                    response.setResponseStatus("404 Not Found");
                }
      
            }
            response.send(outS);
           
           
        } catch (IOException ex) {
            System.out.println("Thread error when reading request");
        }
        try {
            sock.close();
        } catch (IOException ex) {
            System.out.println("CLOSE IOException");
        }

    }
    
    public static HashMap<String,String> getParams(String request){
        String[] split = request.split("&");
        if(split.length > 0){
            HashMap<String,String> params = new HashMap();
            for (String param : split) {
                String[] params_split = param.split("=");
                if(params_split.length > 1){
                    params.put(params_split[0],params_split[1]);
                }
            }
            return params;
        }
        return null;
    }
}
