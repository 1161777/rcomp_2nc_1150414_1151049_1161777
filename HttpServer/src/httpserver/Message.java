package httpserver;

/**
 *
 * @author marcio
 */
class Message {
    
    private String content;

    public Message(String content) {
        this.content = content;
    }

    public String getContent() {
        return this.content;
    }
    
}
