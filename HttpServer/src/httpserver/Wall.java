package httpserver;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author marcio
 */
class Wall {
    
    private String name;
    private ArrayList<Message> messages = new ArrayList();
    private String createdAt;

    Wall(String name) {
       this.name = name;
       this.createdAt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
    }
    
    public void addMessage(Message message){
        messages.add(message);
    }
    
    public Message getMessage(int id){
        return this.messages.get(id);
    }
    
    public Message deleteMessage(int id){
        return this.messages.remove(id);
    }

    public ArrayList<Message> getMessages() {
        return this.messages;
    }

    public String getName() {
        return this.name;
    }
    
    @Override
    public String toString(){
        return "{name: "+ this.name + ",messages_number:"+messages.size()+"}";
    }
    
    public String toJson(){
        String json = "{\"name\": \""+ this.name + "\", \"date\": \""+ this.createdAt + "\",\"messages\":[";
        int i = 1;
        for (Message message : messages) {
            json += "{\"content\":\""+message.getContent()+"\"}";
            if(i != messages.size()){
                json += ",";
            }
            i++;
        }
        json +="]}";
        return json;
    }
    
}
