package httpserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author marcio
 */
public class HttpServer {

    static private final String BASE_FOLDER="www";
    static private ServerSocket sock;

    public static void main(String args[]) throws Exception {
	Socket cliSock;
        int port = Integer.parseInt(args[0]);
       
	if(port < 1024 && port > 65535) {
            System.out.println("Local port number is not valid.");
            System.exit(1);
        }else{
            System.out.println("Server Running on port "+port);
        }
        
	try { 
            sock = new ServerSocket(port); 
        }catch(IOException ex) {
            System.out.println("Server failed to open local port " + port);
            System.exit(1);
        }
        	
        while(true) { 
            cliSock=sock.accept();
            HttpRequestHander request = new HttpRequestHander(cliSock, BASE_FOLDER);
            request.run();
        }
    } 
    
}
